from django.shortcuts import render

from .models import FirstClass

from main.models import Contacts

from main.forms import ContactsForm

def index(request):
    if request.method == 'POST':
        form = ContactsForm(request.POST)
        if form.is_valid():
            form.save()

    form = ContactsForm()
    context = {
       'form': form
    }
    return render(request, 'index.html', context)

