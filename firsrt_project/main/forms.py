from .models import Contacts
from django.forms import ModelForm, TextInput, Textarea


class ContactsForm(ModelForm):
    class Meta:
       model = Contacts 
       fields = ["Name","Mail","Service","Comment"]
       widgets = {
            "Name": TextInput(attrs={
                'class': 'Contacts__frame__input__new_0',
                'placeholder':'Имя'
       }),
            "Mail": TextInput(attrs={
                'class': 'Contacts__frame__input__new',
                'placeholder':'Почта'
       }),
              "Service": TextInput(attrs={
                'class': 'Contacts__frame__input__new_2',
                'placeholder':'Услуга'
       }),
              "Comment": Textarea(attrs={
                'class': 'Contacts__frame__input__new_3',
                'placeholder':'Комментарий к услуге'
       }),
    }