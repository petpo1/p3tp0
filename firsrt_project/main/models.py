from django.db import models


class Contacts(models.Model):
    Name = models.CharField('Имя', max_length=40)
    Mail = models.CharField('Почта', max_length=40)
    Service = models.CharField('Услуга', max_length=40)
    Comment = models.TextField('Комментарий')
